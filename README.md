
# todo_app
An application allows users to create tasks and update completion status.
To add a new task, the user writes the text in TextField and taps the Done button on the keyboard.
To update the completion status of tasks, the user taps the checkbox on every `task` UI.
## Get project from Gitlab
```
cd existing_repo
git remote add origin https://gitlab.com/mr.kieuphu/todoapp-flutter.git
```

## How to run code
```
 cd todoapp-flutter
 flutter pub get
 flutter lib/app/main.dart
```
## How to run unit test and widget test

    flutter test

 1. Run only unit test

    `flutter test test/todo_list_test.dart`

 2. Run only widget test
    `flutter test test/widget_test.dart`
