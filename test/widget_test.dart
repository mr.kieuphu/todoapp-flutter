// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:todo_app/app/main.dart';
import 'package:todo_app/app/reducer/app_reducer.dart';
import 'package:todo_app/app/store/app_state.dart';
import 'package:todo_app/model/item.dart';
import 'package:todo_app/todo_list/store/bottom_bar_state.dart';
import 'package:todo_app/todo_list/store/todo_list_state.dart';
import 'package:todo_app/todo_list/view/item_list.dart';
import 'package:todo_app/todo_list/view/todo_list_page.dart';
import 'package:redux/redux.dart';

void main() {
  group('Find by type', () {
    testWidgets('Test to see that My App widget is in tree',
        (WidgetTester tester) async {
      await tester.pumpWidget(const MyApp());
      expect(find.byType(MaterialApp), findsOneWidget);
    });

    testWidgets('Test to see that widgets of ToDo List Page is in tree',
        (WidgetTester tester) async {
      final Store<AppState> store = Store<AppState>(
        appReducer,
        initialState: AppState.initialState(),
      );
      await tester.pumpWidget(StoreProvider(
        store: store,
        child: const MaterialApp(
          home: ToDoListPage(),
        ),
      ));
      expect(find.byType(TextField), findsOneWidget);
      expect(find.byType(BottomNavigationBar), findsOneWidget);
      expect(find.byType(ItemList), findsWidgets);
    });
  });

  group('Test logic', () {
    testWidgets('test show item list', (WidgetTester tester) async {
      List<Item> items = [
        Item(id: '1', body: 'hihi', isComplete: true),
        Item(id: '2', body: 'haha', isComplete: false),
        Item(id: '3', body: 'hehe', isComplete: false)
      ];
      final Store<AppState> store = Store<AppState>(
        appReducer,
        initialState: AppState(
            todoListState: TodoListState(items: items),
            bottomBarState: const BottomBarState(selectedBarItemIndex: 0)),
      );
      await tester.pumpWidget(StoreProvider(
        store: store,
        child: const MaterialApp(
          home: ToDoListPage(),
        ),
      ));
      expect(find.byKey(const Key('[<\'all\'>]-1-true')), findsOneWidget);
      expect(find.byKey(const Key('[<\'all\'>]-2-false')), findsOneWidget);
      expect(find.byKey(const Key('[<\'all\'>]-3-false')), findsOneWidget);
      expect(find.byKey(const Key('[<\'complete\'>]-1-true')), findsOneWidget);
      expect(
          find.byKey(const Key('[<\'incomplete\'>]-2-false')), findsOneWidget);
      expect(
          find.byKey(const Key('[<\'incomplete\'>]-3-false')), findsOneWidget);
    });
  });

  testWidgets('test update checkbox status in item of All BarItem',
      (WidgetTester tester) async {
    List<Item> items = [
      Item(id: '1', body: 'hihi', isComplete: true),
      Item(id: '2', body: 'haha', isComplete: false),
      Item(id: '3', body: 'hehe', isComplete: false)
    ];
    final Store<AppState> store = Store<AppState>(
      appReducer,
      initialState: AppState(
          todoListState: TodoListState(items: items),
          bottomBarState: const BottomBarState(selectedBarItemIndex: 0)),
    );
    await tester.pumpWidget(StoreProvider(
      store: store,
      child: const MaterialApp(
        home: ToDoListPage(),
      ),
    ));

    var checkboxFinder = find.byKey(const Key('[<\'all\'>]-2-false-checkbox'));
    var checkbox = tester.firstWidget(checkboxFinder) as Checkbox;
    expect(checkbox.value, false);
    await tester.tap(checkboxFinder);
    await tester.pump();

    expect(find.byKey(const Key('[<\'all\'>]-1-true')), findsOneWidget);
    expect(find.byKey(const Key('[<\'all\'>]-2-true')), findsOneWidget);
    expect(find.byKey(const Key('[<\'all\'>]-3-false')), findsOneWidget);
    expect(find.byKey(const Key('[<\'complete\'>]-1-true')), findsOneWidget);
    expect(find.byKey(const Key('[<\'complete\'>]-2-true')), findsOneWidget);
    expect(find.byKey(const Key('[<\'incomplete\'>]-3-false')), findsOneWidget);
  });

  testWidgets('test update checkbox status in item of Complete BarItem',
      (WidgetTester tester) async {
    List<Item> items = [
      Item(id: '1', body: 'hihi', isComplete: true),
      Item(id: '2', body: 'haha', isComplete: true),
      Item(id: '3', body: 'hehe', isComplete: false)
    ];
    final Store<AppState> store = Store<AppState>(
      appReducer,
      initialState: AppState(
          todoListState: TodoListState(items: items),
          bottomBarState: const BottomBarState(selectedBarItemIndex: 1)),
    );
    await tester.pumpWidget(StoreProvider(
      store: store,
      child: const MaterialApp(
        home: ToDoListPage(),
      ),
    ));

    var checkboxFinder =
        find.byKey(const Key('[<\'complete\'>]-2-true-checkbox'));
    var checkbox = tester.firstWidget(checkboxFinder) as Checkbox;
    expect(checkbox.value, true);
    await tester.tap(checkboxFinder);
    await tester.pump();

    expect(find.byKey(const Key('[<\'all\'>]-1-true')), findsOneWidget);
    expect(find.byKey(const Key('[<\'all\'>]-2-false')), findsOneWidget);
    expect(find.byKey(const Key('[<\'all\'>]-3-false')), findsOneWidget);
    expect(find.byKey(const Key('[<\'complete\'>]-1-true')), findsOneWidget);
    expect(find.byKey(const Key('[<\'incomplete\'>]-2-false')), findsOneWidget);
    expect(find.byKey(const Key('[<\'incomplete\'>]-3-false')), findsOneWidget);
  });

  testWidgets('test update checkbox status in item of Incomplete BarItem',
      (WidgetTester tester) async {
    List<Item> items = [
      Item(id: '1', body: 'hihi', isComplete: true),
      Item(id: '2', body: 'haha', isComplete: false),
      Item(id: '3', body: 'hehe', isComplete: false)
    ];
    final Store<AppState> store = Store<AppState>(
      appReducer,
      initialState: AppState(
          todoListState: TodoListState(items: items),
          bottomBarState: const BottomBarState(selectedBarItemIndex: 2)),
    );
    await tester.pumpWidget(StoreProvider(
      store: store,
      child: const MaterialApp(
        home: ToDoListPage(),
      ),
    ));

    var checkboxFinder =
        find.byKey(const Key('[<\'incomplete\'>]-2-false-checkbox'));
    var checkbox = tester.firstWidget(checkboxFinder) as Checkbox;
    expect(checkbox.value, false);
    await tester.tap(checkboxFinder);
    await tester.pump();

    expect(find.byKey(const Key('[<\'all\'>]-1-true')), findsOneWidget);
    expect(find.byKey(const Key('[<\'all\'>]-2-true')), findsOneWidget);
    expect(find.byKey(const Key('[<\'all\'>]-3-false')), findsOneWidget);
    expect(find.byKey(const Key('[<\'complete\'>]-1-true')), findsOneWidget);
    expect(find.byKey(const Key('[<\'complete\'>]-2-true')), findsOneWidget);
    expect(find.byKey(const Key('[<\'incomplete\'>]-3-false')), findsOneWidget);
  });

  testWidgets(
      'test add a new item should add records that complete is false in all and incomplete bottom bar',
      (WidgetTester tester) async {
    final Store<AppState> store = Store<AppState>(
      appReducer,
      initialState: const AppState(
          todoListState: TodoListState(items: []),
          bottomBarState: BottomBarState(selectedBarItemIndex: 0)),
    );
    await tester.pumpWidget(StoreProvider(
      store: store,
      child: const MaterialApp(
        home: ToDoListPage(),
      ),
    ));

    var addItemTextFieldFinder = find.byKey(const Key('add-item-textfield'));
    expect(addItemTextFieldFinder, findsOneWidget);

    var addItemTextField =
        tester.firstWidget(addItemTextFieldFinder) as TextField;
    // test textfield has empty text
    expect(addItemTextField.controller?.text, '');

    await tester.enterText(addItemTextFieldFinder, 'hihihi');
    await tester.testTextInput.receiveAction(TextInputAction.done);
    await tester.pump();

    var listTileFinder = find.byType(ListTile);
    var listTitles = tester.elementList(listTileFinder);
    expect(listTitles.length, 2);
  });
}
