import 'package:flutter_test/flutter_test.dart';
import 'package:todo_app/model/item.dart';
import 'package:todo_app/todo_list/action/bottom_bar_action.dart';
import 'package:todo_app/todo_list/action/todo_list_action.dart';
import 'package:todo_app/todo_list/reducer/bottom_bar_reducer.dart';
import 'package:todo_app/todo_list/reducer/todo_list_reducer.dart';
import 'package:todo_app/todo_list/view/todo_list_page.dart';

void main() {
  // group('todo list reducer', () {
  //   test('should not add new item that title is empty', () {
  //     const title = '';
  //     final action = AddItemAction(title);
  //     expect(action.id, '');

  //     final items = addItemReducer.call([], action);

  //     expect(items.length, 0);
  //   });

  //   test('should add new item that title is not empty', () {
  //     const title = 'hihi';
  //     final action = AddItemAction(title);
  //     expect(action.id, isNotEmpty);

  //     final items = addItemReducer.call([], action);

  //     expect(items.length, 1);
  //     expect(items.first.body, title);
  //   });

  //   test('update complete status of a item', () {
  //     List<Item> initialItems = [
  //       Item(id: '1', body: 'hihi', isComplete: false),
  //       Item(id: '2', body: 'haha', isComplete: false),
  //       Item(id: '3', body: 'hehe', isComplete: false)
  //     ];

  //     Item updateItem = Item(id: '1', body: 'hihi', isComplete: false);
  //     var items = updateCompleteReducer(
  //         initialItems, UpdateItemAction(item: updateItem));
  //     expect(items.length, 3);
  //     expect(items[0].isComplete, true);
  //     expect(items[1].isComplete, false);
  //     expect(items[2].isComplete, false);
  //   });
  // });

  group('bottom bar reducer', () {
    test('test update selected bar item index', () {
      const currentIndex = 0;
      const action = SelectedBarItemAction(index: 1);
      int index = selectedBarItemIndexReducer(currentIndex, action);
      expect(index, 1);
    });
  });

  group('todo view model', () {
    test('complete items that items have isComplete equal true', () {
      List<Item> expectedItems = [
        Item(id: '2', body: 'haha', isComplete: true),
        Item(id: '3', body: 'hehe', isComplete: true)
      ];

      List<Item> initialItems = [
        Item(id: '1', body: 'hihi', isComplete: false),
        ...expectedItems
      ];

      var model = ToDoViewModel.fixture(initialItems);
      var completeItems = model.completeItems();
      expect(completeItems.length, 2);
      expect(completeItems, expectedItems);
    });

    test('incomplete items that items have isComplete equal false', () {
      List<Item> expectedItems = [
        Item(id: '2', body: 'haha', isComplete: false),
        Item(id: '3', body: 'hehe', isComplete: false)
      ];

      List<Item> initialItems = [
        Item(id: '1', body: 'hihi', isComplete: true),
        ...expectedItems
      ];

      var model = ToDoViewModel.fixture(initialItems);
      var incompleteItems = model.incompleteItems();
      expect(incompleteItems.length, 2);
      expect(incompleteItems, expectedItems);
    });
  });
}
