import 'package:todo_app/model/item.dart';
import 'package:uuid/uuid.dart';

class GetTodoListAction {}

class LoadedTodoListAction {
  final List<Item> items;

  const LoadedTodoListAction({required this.items});
}

class AddItemAction {
  static var uuid = const Uuid();
  String _id = '';
  final String item;

  AddItemAction(this.item) {
    if (item.isNotEmpty) {
      _id = uuid.v1();
    }
  }

  String get id => _id;
}

class AddItemSuccessAction {
  final List<Item> items;

  AddItemSuccessAction({required this.items});
}

class UpdateItemAction {
  final Item item;

  UpdateItemAction({required this.item});
}

class UpdateItemSuccessAction {
  final List<Item> items;
  UpdateItemSuccessAction({required this.items});
}

class SaveItemErrorAction {
  final String text;

  SaveItemErrorAction({required this.text});
}
