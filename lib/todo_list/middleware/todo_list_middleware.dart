import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:redux/redux.dart';
import 'package:todo_app/model/item.dart';
import 'package:todo_app/todo_list/action/todo_list_action.dart';
import 'package:todo_app/todo_list/store/todo_list_state.dart';
import 'package:todo_app/app/store/app_state.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

// List<Middleware<AppState>> todoListStateMiddleware() {
//   final loadItems = _loadFromPrefs();
//   final saveItems = _saveToPrefs();

//   return [
//     TypedMiddleware<AppState, AddItemAction>(saveItems),
//     TypedMiddleware<AppState, UpdateItemAction>(saveItems),
//     TypedMiddleware<AppState, GetTodoListAction>(loadItems),
//   ];
// }

// Middleware<AppState> _loadFromPrefs() {
//   return (Store<AppState> store, action, NextDispatcher next) {
//     next(action);
//     loadTodoListState().then(
//         (state) => store.dispatch(LoadedTodoListAction(items: state.items)));
//   };
// }

// Middleware<AppState> _saveToPrefs() {
//   return (Store<AppState> store, action, NextDispatcher next) {
//     next(action);
//     saveTodoListState(store.state.todoListState);
//   };
// }

final todoListEpics =
    combineEpics<AppState>([addItemEpic, updateItemEpic, loadItemsEpic]);

Future<bool> saveTodoListState(TodoListState state) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  var string = json.encode(state.toJson());
  return await preferences.setString('todoListState', string);
}

Future<TodoListState> loadTodoListState() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  var string = preferences.getString('todoListState');
  if (string != null) {
    Map map = json.decode(string);
    return TodoListState.fromJson(map);
  }
  return const TodoListState(items: []);
}

Stream<dynamic> addItemEpic(
    Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions
      .whereType<AddItemAction>()
      .switchMap((action) => _addItem(store.state.todoListState, action));
}

Stream<dynamic> updateItemEpic(
    Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions
      .whereType<UpdateItemAction>()
      .switchMap((action) => _updateItem(store.state.todoListState, action));
}

Stream<dynamic> loadItemsEpic(
    Stream<dynamic> actions, EpicStore<AppState> store) {
  return actions
      .whereType<GetTodoListAction>()
      .switchMap((action) => _loadToDoList());
}

Stream<dynamic> _addItem(TodoListState state, AddItemAction action) async* {
  var items = state.items;
  if (action.item.isNotEmpty) {
    items = [
      ...state.items,
      Item(id: action.id, body: action.item, isComplete: false)
    ];
  }
  var newState = TodoListState(items: items);
  await saveTodoListState(newState);
  yield AddItemSuccessAction(items: items);
}

Stream<dynamic> _updateItem(
    TodoListState state, UpdateItemAction action) async* {
  var items = state.items
      .map((item) => item.id == action.item.id
          ? item.copyWith(isComplete: !item.isComplete)
          : item)
      .toList();
  var newState = TodoListState(items: items);
  await saveTodoListState(newState);
  yield UpdateItemSuccessAction(items: items);
}

Stream<dynamic> _loadToDoList() async* {
  var result = await loadTodoListState();
  yield LoadedTodoListAction(items: result.items);
  // yield Stream.fromFuture(loadTodoListState().then((value) => LoadedTodoListAction(items: value.items)));
}
