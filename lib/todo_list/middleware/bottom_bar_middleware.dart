import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:redux/redux.dart';
import 'package:todo_app/todo_list/action/bottom_bar_action.dart';
import 'package:todo_app/todo_list/store/bottom_bar_state.dart';
import 'package:todo_app/app/store/app_state.dart';

List<Middleware<AppState>> bottomBarStateMiddleware() {
  final loadItems = _loadFromPrefs();
  final saveItems = _saveToPrefs();

  return [
    TypedMiddleware<AppState, SelectedBarItemAction>(saveItems),
    TypedMiddleware<AppState, GetBarItemIndexAction>(loadItems),
  ];
}

Middleware<AppState> _loadFromPrefs() {
  return (Store<AppState> store, action, NextDispatcher next) {
    next(action);
    loadBottomBarState().then((state) => store
        .dispatch(SelectedBarItemAction(index: state.selectedBarItemIndex)));
  };
}

Middleware<AppState> _saveToPrefs() {
  return (Store<AppState> store, action, NextDispatcher next) {
    next(action);
    saveBottomBarState(store.state.bottomBarState);
  };
}

void saveBottomBarState(BottomBarState state) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  var string = json.encode(state.toJson());
  await preferences.setString('bottomBarState', string);
}

Future<BottomBarState> loadBottomBarState() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  var string = preferences.getString('bottomBarState');
  if (string != null) {
    Map map = json.decode(string);
    return BottomBarState.fromJson(map);
  }
  return const BottomBarState(selectedBarItemIndex: 0);
}
