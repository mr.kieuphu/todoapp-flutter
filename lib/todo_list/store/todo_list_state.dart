import 'package:todo_app/model/item.dart';
import 'package:meta/meta.dart';
import 'package:collection/collection.dart';

@immutable
class TodoListState {
  final List<Item> items;

  const TodoListState({
    required this.items,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TodoListState &&
          runtimeType == other.runtimeType &&
          const ListEquality().equals(items, other.items);

  @override
  int get hashCode => items.hashCode;

  factory TodoListState.fromJson(Map json) {
    return TodoListState(
        items: (json['items'] as List).map((i) => Item.fromJson(i)).toList());
  }

  Map toJson() => {
        'items': items,
      };
}
