import 'package:meta/meta.dart';

@immutable
class BottomBarState {
  final int selectedBarItemIndex;

  const BottomBarState({
    required this.selectedBarItemIndex,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BottomBarState &&
          runtimeType == other.runtimeType &&
          selectedBarItemIndex == other.selectedBarItemIndex;

  @override
  int get hashCode => selectedBarItemIndex.hashCode;

  factory BottomBarState.fromJson(Map json) {
    return BottomBarState(
        selectedBarItemIndex: (json['selected_bar_item_index']));
  }

  Map toJson() => {'selected_bar_item_index': selectedBarItemIndex};
}
