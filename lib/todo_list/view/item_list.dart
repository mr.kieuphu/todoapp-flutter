import 'package:flutter/material.dart';
import 'package:todo_app/model/item.dart';

class ItemList extends StatelessWidget {
  const ItemList({required this.model, Key? key}) : super(key: key);
  final ItemViewModel model;

  @override
  Widget build(BuildContext context) {
    if (model.items.isNotEmpty) {
      return ListView(
        children: model.items
            .map((e) => ListTile(
                  key: Key(key.toString() +
                      '-' +
                      e.id +
                      '-' +
                      e.isComplete.toString()),
                  title: Text(e.body),
                  trailing: Checkbox(
                    key: Key(key.toString() +
                        '-' +
                        e.id +
                        '-' +
                        e.isComplete.toString() +
                        '-checkbox'),
                    value: e.isComplete,
                    onChanged: (b) {
                      model.onUpdateCompleteItem?.call(e);
                    },
                  ),
                ))
            .toList(),
      );
    } else {
      return const Center(
        child: Text('List is empty'),
      );
    }
  }
}

class ItemViewModel {
  final List<Item> items;
  final Function(Item)? onUpdateCompleteItem;

  ItemViewModel({
    required this.items,
    required this.onUpdateCompleteItem,
  });
}
