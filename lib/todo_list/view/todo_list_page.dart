import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:todo_app/model/item.dart';
import 'package:todo_app/app/store/app_state.dart';
import 'package:redux/redux.dart';
import 'package:todo_app/todo_list/action/todo_list_action.dart';
import 'package:todo_app/todo_list/action/bottom_bar_action.dart';
import 'package:todo_app/todo_list/view/item_list.dart';

class ToDoListPage extends StatefulWidget {
  const ToDoListPage({Key? key}) : super(key: key);

  @override
  _ToDoListPageState createState() => _ToDoListPageState();
}

class _ToDoListPageState extends State<ToDoListPage> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ToDoViewModel>(
      onInit: (store) {
        store.dispatch(GetTodoListAction());
        store.dispatch(GetBarItemIndexAction());
      },
      converter: (store) => ToDoViewModel.create(store),
      distinct: true,
      builder: (context, viewModel) => Scaffold(
        appBar: AppBar(
          title: const Text('ToDo Application'),
        ),
        body: Column(
          children: [
            AddItemWidget(
              model: viewModel,
            ),
            Expanded(
              child: IndexedStack(
                index: viewModel.selectedBarItemIndex,
                children: [
                  ItemList(
                    key: const Key('all'),
                    model: ItemViewModel(
                      items: viewModel.items,
                      onUpdateCompleteItem: viewModel.onUpdateCompleteItem,
                    ),
                  ),
                  ItemList(
                    key: const Key('complete'),
                    model: ItemViewModel(
                      items: viewModel.completeItems(),
                      onUpdateCompleteItem: viewModel.onUpdateCompleteItem,
                    ),
                  ),
                  ItemList(
                    key: const Key('incomplete'),
                    model: ItemViewModel(
                      items: viewModel.incompleteItems(),
                      onUpdateCompleteItem: viewModel.onUpdateCompleteItem,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.list),
              label: 'All',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.list),
              label: 'Complete',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.list),
              label: 'Incomplete',
            ),
          ],
          currentIndex: viewModel.selectedBarItemIndex,
          onTap: viewModel.selectedBarItem,
        ),
      ),
    );
  }
}

class AddItemWidget extends StatefulWidget {
  final ToDoViewModel model;
  const AddItemWidget({required this.model, Key? key}) : super(key: key);

  @override
  _AddItemWidgetState createState() => _AddItemWidgetState();
}

class _AddItemWidgetState extends State<AddItemWidget> {
  final _textController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(12.0),
      child: TextField(
        key: const Key('add-item-textfield'),
        controller: _textController,
        decoration: const InputDecoration.collapsed(
            hintText: 'Add an item', border: UnderlineInputBorder()),
        onSubmitted: (text) {
          widget.model.onAddItem?.call(text);
          _textController.clear();
        },
      ),
    );
  }
}

class ToDoViewModel {
  final List<Item> items;
  final int selectedBarItemIndex;
  final Function(int)? selectedBarItem;
  final Function(String)? onAddItem;
  final Function(Item)? onUpdateCompleteItem;

  ToDoViewModel({
    required this.items,
    required this.selectedBarItemIndex,
    required this.selectedBarItem,
    required this.onAddItem,
    required this.onUpdateCompleteItem,
  });

  factory ToDoViewModel.fixture(
      [List<Item> items = const [],
      int selectedBarItemIndex = 0,
      Function(int)? selectedBarItem,
      Function(String)? onAddItem,
      Function(Item)? onUpdateCompleteItem]) {
    return ToDoViewModel(
      items: items,
      selectedBarItemIndex: selectedBarItemIndex,
      selectedBarItem: selectedBarItem,
      onAddItem: onAddItem,
      onUpdateCompleteItem: onUpdateCompleteItem,
    );
  }

  factory ToDoViewModel.create(Store<AppState> store) {
    _selectedBarItem(int body) {
      store.dispatch(SelectedBarItemAction(index: body));
    }

    _onAddItem(String body) {
      store.dispatch(AddItemAction(body));
    }

    _onUpdateCompleteItem(Item item) {
      store.dispatch(UpdateItemAction(item: item));
    }

    return ToDoViewModel(
      items: store.state.todoListState.items,
      selectedBarItemIndex: store.state.bottomBarState.selectedBarItemIndex,
      selectedBarItem: _selectedBarItem,
      onAddItem: _onAddItem,
      onUpdateCompleteItem: _onUpdateCompleteItem,
    );
  }

  List<Item> completeItems() {
    return items.where((element) => element.isComplete == true).toList();
  }

  List<Item> incompleteItems() {
    return items.where((element) => element.isComplete == false).toList();
  }
}
