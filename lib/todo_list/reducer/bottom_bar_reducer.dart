import 'package:redux/redux.dart';
import 'package:todo_app/todo_list/action/bottom_bar_action.dart';
import 'package:todo_app/todo_list/store/bottom_bar_state.dart';

BottomBarState bottomBarReducer(BottomBarState state, dynamic action) {
  return BottomBarState(
    selectedBarItemIndex:
        selectedBarItemIndexReducer(state.selectedBarItemIndex, action),
  );
}

final selectedBarItemIndexReducer =
    TypedReducer<int, SelectedBarItemAction>(_selectedBarItemReducer);

int _selectedBarItemReducer(int index, SelectedBarItemAction action) {
  return action.index;
}
