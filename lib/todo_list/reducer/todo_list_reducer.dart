import 'package:todo_app/todo_list/action/todo_list_action.dart';
import 'package:todo_app/todo_list/store/todo_list_state.dart';
import 'package:redux/redux.dart';
import 'package:todo_app/model/item.dart';

TodoListState todoListReducer(TodoListState state, dynamic action) {
  return TodoListState(
    items: itemReducer(state.items, action),
  );
}

Reducer<List<Item>> itemReducer = combineReducers<List<Item>>([
  loadedTodoListReducer,
  addItemSuccessReducer,
  updateItemSuccessReducer,
]);

final loadedTodoListReducer =
    TypedReducer<List<Item>, LoadedTodoListAction>(_loadedTodoListReducer);
List<Item> _loadedTodoListReducer(
    List<Item> items, LoadedTodoListAction action) {
  return action.items;
}

final addItemSuccessReducer =
    TypedReducer<List<Item>, AddItemSuccessAction>(_addItemSuccessReducer);
List<Item> _addItemSuccessReducer(
    List<Item> items, AddItemSuccessAction action) {
  return action.items;
}

final updateItemSuccessReducer =
    TypedReducer<List<Item>, UpdateItemSuccessAction>(
        _updateItemSuccessReducer);
List<Item> _updateItemSuccessReducer(
    List<Item> items, UpdateItemSuccessAction action) {
  return action.items;
}
