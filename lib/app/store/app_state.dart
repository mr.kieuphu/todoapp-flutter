import 'package:flutter/material.dart';
import 'package:todo_app/model/item.dart';
import 'package:todo_app/todo_list/store/bottom_bar_state.dart';
import 'package:todo_app/todo_list/store/todo_list_state.dart';

@immutable
class AppState {
  final TodoListState todoListState;
  final BottomBarState bottomBarState;

  const AppState({
    required this.todoListState,
    required this.bottomBarState,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppState &&
          runtimeType == other.runtimeType &&
          todoListState == other.todoListState &&
          bottomBarState == other.bottomBarState;

  @override
  int get hashCode => todoListState.hashCode ^ bottomBarState.hashCode;

  factory AppState.initialState() {
    return const AppState(
      todoListState: TodoListState(items: []),
      bottomBarState: BottomBarState(selectedBarItemIndex: 0),
    );
  }

  factory AppState.fromJson(Map json) {
    final _todoListState = TodoListState(
        items: (json['items'] as List).map((i) => Item.fromJson(i)).toList());
    final _bottomBarState =
        BottomBarState(selectedBarItemIndex: (json['selected_bar_item_index']));
    return AppState(
        todoListState: _todoListState, bottomBarState: _bottomBarState);
  }

  Map toJson() => {
        'items': todoListState.items,
        'selected_bar_item_index': bottomBarState.selectedBarItemIndex
      };
}
