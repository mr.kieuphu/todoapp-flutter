import 'package:flutter/material.dart';
import 'package:todo_app/todo_list/middleware/bottom_bar_middleware.dart';
import 'package:todo_app/todo_list/middleware/todo_list_middleware.dart';
import 'package:todo_app/todo_list/view/todo_list_page.dart';
import 'package:todo_app/app/store/app_state.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:todo_app/app/reducer/app_reducer.dart';
import 'package:redux_epics/redux_epics.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Store<AppState> store = Store<AppState>(
      appReducer,
      initialState: AppState.initialState(),
      middleware: [
        ...[EpicMiddleware<AppState>(todoListEpics)],
        ...bottomBarStateMiddleware(),
      ],
    );

    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        title: 'ToDo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const ToDoListPage(),
      ),
    );
  }
}
