import 'package:todo_app/todo_list/reducer/bottom_bar_reducer.dart';
import 'package:todo_app/todo_list/reducer/todo_list_reducer.dart';
import 'package:todo_app/app/store/app_state.dart';

AppState appReducer(AppState state, action) {
  return AppState(
    todoListState: todoListReducer(state.todoListState, action),
    bottomBarState: bottomBarReducer(state.bottomBarState, action),
  );
}
