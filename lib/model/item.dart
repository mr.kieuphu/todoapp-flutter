class Item {
  final String id;
  final String body;
  final bool isComplete;

  Item({
    required this.id,
    required this.body,
    required this.isComplete,
  });

  Item copyWith({String? id, String? body, bool? isComplete}) {
    return Item(
        id: id ?? this.id,
        body: body ?? this.body,
        isComplete: isComplete ?? this.isComplete);
  }

  Item.fromJson(Map json)
      : id = json['id'],
        body = json['body'],
        isComplete = json['is_complete'];

  Map toJson() => {
        'id': id,
        'body': body,
        'is_complete': isComplete,
      };

  static fromJSon(i) {}
}
